#
# Be sure to run `pod lib lint YLWSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'YLWSDK'
  s.version          = '0.1.0'
  s.summary          = 'A short description of YLWSDK.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/YuanLiwei/ylwsdk.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'XiaoY2017' => 'yuanliwei@bjca.org.cn' }
  s.source           = { :git => 'https://gitlab.com/YuanLiwei/ylwsdk.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

    # s.source_files = 'YLWSDK/Classes/**/*'
    
  s.default_subspecs = 'YLWCore'
  
  s.subspec 'YLWCore' do |core|
      core.dependency 'YLWSDK/YLWFoundation'
      core.vendored_frameworks = 'YLWSDK/YLWCore.framework'
  end
  
  s.subspec 'YLWFoundation' do |foundation|
      foundation.vendored_frameworks = 'YLWSDK/Support/Foundation/YLWFoundation.framework'
  end
  
  s.subspec 'YLWOldSDK' do |oldSdk|
        oldSdk.dependency 'YLWSDK/YLWCore'
        oldSdk.vendored_frameworks = 'YLWSDK/Support/Old/YLWOldSDK/YLWOldSDK.framework'
  end
  
  # s.resource_bundles = {
  #   'YLWSDK' => ['YLWSDK/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
