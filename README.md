# YLWSDK

[![CI Status](https://img.shields.io/travis/XiaoY2017/YLWSDK.svg?style=flat)](https://travis-ci.org/XiaoY2017/YLWSDK)
[![Version](https://img.shields.io/cocoapods/v/YLWSDK.svg?style=flat)](https://cocoapods.org/pods/YLWSDK)
[![License](https://img.shields.io/cocoapods/l/YLWSDK.svg?style=flat)](https://cocoapods.org/pods/YLWSDK)
[![Platform](https://img.shields.io/cocoapods/p/YLWSDK.svg?style=flat)](https://cocoapods.org/pods/YLWSDK)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YLWSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'YLWSDK'
```

## Author

XiaoY2017, yuanliwei@bjca.org.cn

## License

YLWSDK is available under the MIT license. See the LICENSE file for more info.
