//
//  YLWLog.h
//  YLWFoundation
//
//  Created by szyx on 2021/3/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YLWLog : NSObject

+(void)logMessage:(NSString *)message;

@end

NS_ASSUME_NONNULL_END
