//
//  YLWFoundation.h
//  YLWFoundation
//
//  Created by szyx on 2021/3/21.
//

#import <Foundation/Foundation.h>

//! Project version number for YLWFoundation.
FOUNDATION_EXPORT double YLWFoundationVersionNumber;

//! Project version string for YLWFoundation.
FOUNDATION_EXPORT const unsigned char YLWFoundationVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <YLWFoundation/PublicHeader.h>

#import <YLWFoundation/YLWLog.h>

