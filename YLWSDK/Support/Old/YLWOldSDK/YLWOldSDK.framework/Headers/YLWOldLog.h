//
//  YLWOldLog.h
//  YLWOldSDK
//
//  Created by szyx on 2021/3/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YLWOldLog : NSObject

+(void)oldLog:(NSString *)message;

+(void)oldFoundationLog:(NSString *)message;

@end

NS_ASSUME_NONNULL_END
