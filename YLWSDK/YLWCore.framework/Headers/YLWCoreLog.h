//
//  YLWCoreLog.h
//  YLWCore
//
//  Created by szyx on 2021/3/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YLWCoreLog : NSObject

+(void)coreLog:(NSString *)message;

@end

NS_ASSUME_NONNULL_END
