//
//  YLWCore.h
//  YLWCore
//
//  Created by szyx on 2021/3/21.
//

#import <Foundation/Foundation.h>

//! Project version number for YLWCore.
FOUNDATION_EXPORT double YLWCoreVersionNumber;

//! Project version string for YLWCore.
FOUNDATION_EXPORT const unsigned char YLWCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <YLWCore/PublicHeader.h>
#import <YLWCore/YLWCoreLog.h>

